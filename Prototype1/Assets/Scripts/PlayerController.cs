using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private float acceleration = 0.2f;
    private float forwardInput;
    private bool forwardButton;
    private bool resetButton;
    private bool reverseButton;
    private float horizontalInput;
    private float maxSpeed = 20.0f;
    private float noInputSpeedChange = 0.5f;
    private float speed = 5.0f;
    private float turnSpeed = 25f;
    private Vector3 defaultVehiclePosition = new Vector3(0, 1.5f, 0);
    private Quaternion defaultVehicleRotation = Quaternion.Euler(0, 0, 0);

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        GetInputs();
        HandleSpeedInput();
        // move the vehicle forward
        transform.Translate(Vector3.forward * Time.deltaTime * speed);
        // turn the vehicle, but only if it's moving forward or backward
        if (speed != 0) {
            transform.Rotate(Vector3.up, turnSpeed * horizontalInput * Time.deltaTime);
        }
        if (resetButton) {
            ResetPosition();
        }
    }

    void GetInputs() {
        horizontalInput = Input.GetAxis("Horizontal");
        forwardInput = Input.GetAxis("Vertical");
        forwardButton = Input.GetButton("Fire1");
        reverseButton = Input.GetButton("Fire2");
        resetButton = Input.GetButton("Fire3");
    }

    void HandleSpeedInput() {
        if (forwardButton || forwardInput > 0) {
            // We'll move the vehicle forward
            // move forward if not at max speed
            if (speed < maxSpeed) {
                speed = speed + acceleration;
            }
            // if over max speed, reset to max speed
            if (speed > maxSpeed) {
                speed = maxSpeed;
            }
        } else if((!forwardButton && reverseButton) || forwardInput < 0) {
            // but if reverse is pressed and forward isn't
            // move it backward
            speed = speed - acceleration;
            if (reverseButton && speed < -maxSpeed) {
                speed = -maxSpeed;
            }
        } else {
            Debug.Log("No press, reducing");
            ReduceSpeed();
        }
    }

    void ReduceSpeed() {
        // reset 
        if (speed > 0) {
            speed -= noInputSpeedChange;
        } else if (speed < 0) {
            speed += noInputSpeedChange;
        }
        // avoid oscillations
        if (Mathf.Abs(speed) < noInputSpeedChange) {
            speed = 0;
        }
    }

    void ResetPosition() {
        transform.position = defaultVehiclePosition;
        transform.rotation = defaultVehicleRotation;
    }
}
