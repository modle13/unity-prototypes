﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControllerX : MonoBehaviour
{
    public float speed;
    public float rotationSpeed;
    public float verticalInput;
    public float horizontalInput;
    private float maxSpeed = 60f;
    private float minSpeed = 15f;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        // get the user's vertical input
        verticalInput = Input.GetAxis("Vertical");
        horizontalInput = Input.GetAxis("Horizontal");

        // if horizontal input is provided, accelerate up to max speed, else decelerate down to min speed
        if (horizontalInput > 0) {
            if (speed < maxSpeed) {
                speed += 0.5f;
            }
        } else {
            if (speed > minSpeed) {
                speed -= 0.5f;
            } else {
                speed = minSpeed;
            }
        }

        // move the plane forward at a constant rate
        transform.Translate(Vector3.forward * Time.deltaTime * speed);

        // tilt the plane up/down based on up/down arrow keys
        transform.Rotate(Vector3.right * Time.deltaTime * rotationSpeed * verticalInput);
    }
}
