﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControllerX : MonoBehaviour
{
    public GameObject dogPrefab;
    private float lastSpawn;
    private float spawnDelay = 1.0f;

    void Start() {
        // set negative so dog can be spawned immediately
        lastSpawn = -10f;
    }

    // Update is called once per frame
    void Update()
    {
        // On spacebar press, send dog
        bool canSpawn = Time.time - lastSpawn > spawnDelay;
        if (Input.GetKeyDown(KeyCode.Space) && canSpawn)
        {
            Instantiate(dogPrefab, transform.position, dogPrefab.transform.rotation);
            lastSpawn = Time.time;
        }
    }
}
