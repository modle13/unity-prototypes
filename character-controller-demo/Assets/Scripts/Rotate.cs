using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotate : MonoBehaviour
{
    public float xAngle, yAngle, zAngle;
    public float maxRot = 0.5f;
    
    private GameObject cube;

    void Update()
    {
        if (Time.frameCount % 100 == 0)
        {
            xAngle = Random.Range(-maxRot, maxRot);
            yAngle = Random.Range(-maxRot, maxRot);
            zAngle = Random.Range(-maxRot, maxRot);
        }
        transform.Rotate(xAngle, yAngle, zAngle, Space.Self);
    }

    void OnGUI()
    {
        GUI.contentColor = Color.black;
        GUI.Label(new Rect(100, 0, 100, 50), (Time.frameCount % 100).ToString());
        GUI.Label(new Rect(100, 20, 100, 50), Mathf.Floor(Time.frameCount / 100).ToString());
    }
}
