using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

using Photon.Pun;
using TMPro;

public class Loading : MonoBehaviour
{

    public string newGameScene;
    public int loadTime;
    private int loadStartTime = 0;

    private TextMeshProUGUI loadMessage;
    private TextMeshProUGUI timer;
    public List<string> messages = new List<string>()
    {
        "beveling the edges",
        "carpeting the stairs",
        "removing the carpeting from the stairs",
        "checking for spare handrails",
        "securing the perimeter",
        "generating dungeons",
        "formulating orbital mechanics",
        "estimating distance to the coffee shop",
        "pondering the heat death of the universe",
        "reading the tea leaves"
    };

    void Start()
    {
        loadMessage = transform.Find("Message").GetComponent<TextMeshProUGUI>();
        int randomIndex = Random.Range(0, messages.Count);
        loadMessage.text = messages[randomIndex];

        timer = transform.Find("Timer").GetComponent<TextMeshProUGUI>();

        loadStartTime = Time.frameCount;
    }

    void Update()
    {
        if ((Time.frameCount - loadStartTime) > loadTime)
        {
            // whenever loading a multiplayer scene, need to use `PhotonNetwork.LoadLevel(sceneName);`
            PhotonNetwork.LoadLevel(newGameScene);
            //SceneManager.LoadScene(newGameScene);
        }

        int timeLeft = loadTime - (Time.frameCount - loadStartTime);
        timer.text = (Mathf.Clamp(Mathf.Floor((timeLeft)/60), 0, loadTime)).ToString();

        if (Time.frameCount % 60 == 0 && messages.Count > 0)
        {
            int randomIndex = Random.Range(0, messages.Count);
            loadMessage.text = messages[randomIndex];
            messages.RemoveAt(randomIndex);
        }
    }
}