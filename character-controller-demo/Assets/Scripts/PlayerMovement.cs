using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

// https://www.youtube.com/watch?v=_QajrabyTJc

public class PlayerMovement : MonoBehaviour
{
    public CharacterController controller;
    public float speed = 12.0f;
    public float gravity = -9.81f;
    public float jumpHeight = 3.0f;

    public Transform groundCheck;
    public float groundDistance = 0.4f;
    // layers to apply to
    public LayerMask groundMask;

    Vector3 velocity;
    bool isGrounded;

    PhotonView view;

    void Start()
    {
        view = GetComponent<PhotonView>();
    }

    // Update is called once per frame
    void Update()
    {
        if (view.IsMine) {
            isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);

            if (isGrounded && velocity.y < 0) {
                // keeping this -2f and not 0 will force the player down to the ground rather than stopping it too high
                velocity.y = -2f;
            }

            float xMovement = Input.GetAxis("Horizontal");
            float zMovement = Input.GetAxis("Vertical");
            // add right and forward vectors together
            Vector3 move = transform.right * xMovement + transform.forward * zMovement;

            // use reference to controller to move it
            controller.Move(move * speed * Time.deltaTime);

            if (Input.GetButtonDown("Jump") && isGrounded) {
                velocity.y = Mathf.Sqrt(jumpHeight * -2 * gravity);
            }

            velocity.y += gravity * Time.deltaTime;

            // multiply by Time.deltaTime again because that's how gravity works
            controller.Move(velocity * Time.deltaTime);
        }
    }
}
