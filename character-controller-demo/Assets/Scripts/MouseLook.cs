using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

// https://www.youtube.com/watch?v=_QajrabyTJc

public class MouseLook : MonoBehaviour
{
    public float mouseSensitivity = 200.0f;
    public Transform playerBody;
    float xRotation = 0f;

    PhotonView view;

    void Start()
    {
        view = GetComponent<PhotonView>();
        Cursor.lockState = CursorLockMode.Locked;
    }

    void Update()
    {
        if (view.IsMine) {
            // Time.deltaTime is the amount of time since the last Update call
            float mouseX = Input.GetAxis("Mouse X") * mouseSensitivity * Time.deltaTime;
            float mouseY = Input.GetAxis("Mouse Y") * mouseSensitivity * Time.deltaTime;

            xRotation -= mouseY;
            // prevents over-rotation which would case camera to look behind player
            xRotation = Mathf.Clamp(xRotation, -90f, 90f);

            // look up and down by changing camera transform rotation
            transform.localRotation = Quaternion.Euler(xRotation, 0f, 0f);
            // look left and right
            playerBody.Rotate(Vector3.up * mouseX);
        }
    }

}
