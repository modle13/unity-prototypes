using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using TMPro;
using UnityEngine.SceneManagement;


public class CreateAndJoinRooms : MonoBehaviourPunCallbacks
{

    public GameObject createInput;
    public GameObject joinInput;

    public void CreateRoom()
    {
        // also automatically joins the room
        Debug.Log("Creating room " + createInput.GetComponent<TMP_InputField>().text);
        PhotonNetwork.CreateRoom(createInput.GetComponent<TMP_InputField>().text);
    }

    public void JoinRoom()
    {
        Debug.Log("Joining room " + joinInput.GetComponent<TMP_InputField>().text);
        PhotonNetwork.JoinRoom(joinInput.GetComponent<TMP_InputField>().text);
    }

    public override void OnJoinedRoom()
    {
        // Note: by going through the "Loading" scene, then to the "Level1" scene from there, sync between
        // players does not properly work
        // SceneManager.LoadScene("Loading");
        PhotonNetwork.LoadLevel("Level1");
    }
}
