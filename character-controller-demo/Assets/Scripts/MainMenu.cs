using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public string loadingScene;

    public void NewGame()
    {
        SceneManager.LoadScene(loadingScene);
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
