using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OncomingController : MonoBehaviour
{
    private float speed;
    private Quaternion defaultVehicleRotation = Quaternion.Euler(0, 180f, 0);
    private float minXPoint = -10f;
    private float maxXPoint = 72f;
    private float minYPoint = -5f;
    private float startZPosition = 95f;
    private float resetZPoint = -80f;
    private Vector3 originalPosition;
    private float minSpeed = 10f;
    private float maxSpeed = 25f;

    // Start is called before the first frame update
    void Start()
    {
        speed = GetSpeed();
        // speed = 30f;
        originalPosition = new Vector3(transform.position.x, transform.position.y, transform.position.z);
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector3.forward * Time.deltaTime * speed);
        // Debug.Log("position is " + transform.position.z);
        int currentZ = (int)transform.position.z;
        if (
            currentZ > startZPosition
            || currentZ < resetZPoint
            || transform.position.x < minXPoint
            || transform.position.x > maxXPoint
            || transform.position.y < minYPoint
        ) {
            // Debug.Log("resetting position " + transform.position + "; reset is " + resetZPoint + "; start is " + startZPosition + "; rounded current is " + currentZ);
            // -2f accounts for offset to avoid repeated trigger on first reset
            transform.position = new Vector3(originalPosition.x, originalPosition.y, startZPosition - 5f);
            transform.rotation = defaultVehicleRotation;
        }
    }

    float GetSpeed() {
        return Random.Range(minSpeed, maxSpeed);
    }
}
