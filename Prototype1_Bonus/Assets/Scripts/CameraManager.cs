using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour
{
    public GameObject player1CameraFront;
    public GameObject player1CameraRear;
    public GameObject player2CameraFront;
    public GameObject player2CameraRear;

    // Start is called before the first frame update
    void Start()
    {
        player1CameraFront.active = true;
        player1CameraRear.active = false;
        player2CameraFront.active = true;
        player2CameraRear.active = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Player1CameraToggle"))
        {
            player1CameraFront.active = !player1CameraFront.active;
            player1CameraRear.active = !player1CameraRear.active;
        }
        if (Input.GetButtonDown("Player2CameraToggle"))
        {
            player2CameraFront.active = !player2CameraFront.active;
            player2CameraRear.active = !player2CameraRear.active;
        }
    }
}
