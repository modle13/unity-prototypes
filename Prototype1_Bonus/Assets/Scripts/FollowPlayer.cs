using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour
{
    public GameObject player;
    public string position;
    private Vector3 offset;
    private Vector3 behind = new Vector3(0, 10, -15);
    private Vector3 ahead = new Vector3(0, 10, 15);
    private bool enabled = false;

    // Start is called before the first frame update
    void Start()
    {
        if (position == "ahead") {
            offset = ahead;
        } else {
            offset = behind;
        }
    }

    // Update is called once per frame
    void Update()
    {
    }

    /// <summary>
    /// LateUpdate is called every frame, if the Behaviour is enabled.
    /// It is called after all Update functions have been called.
    /// </summary>
    void LateUpdate()
    {
        // offset the camera using the player's position
        transform.position = player.transform.position + offset;
    }
}
