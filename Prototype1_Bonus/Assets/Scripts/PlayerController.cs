using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private float acceleration = 0.2f;
    private float forwardInput;
    private bool resetPositionButton;
    private bool resetOrientationButton;
    private float horizontalInput;
    private float maxSpeed = 20.0f;
    private float noInputSpeedChange = 0.5f;
    private float speed = 5.0f;
    private float turnSpeed = 40f;
    private Vector3 defaultVehiclePosition;
    private Quaternion defaultVehicleRotation = Quaternion.Euler(0, 0, 0);

    // Start is called before the first frame update
    void Start()
    {
        // use coordinates to make a new Vector avoid linking refs
        defaultVehiclePosition = new Vector3(transform.position.x, transform.position.y, transform.position.z);
    }

    // Update is called once per frame
    void Update()
    {
        GetInputs();
        HandleSpeedInput();
        // move the vehicle forward
        transform.Translate(Vector3.forward * Time.deltaTime * speed);
        // turn the vehicle, but only if it's moving forward or backward
        if (speed != 0) {
            transform.Rotate(Vector3.up, turnSpeed * horizontalInput * Time.deltaTime);
        }
        if (resetPositionButton) {
            ResetVehicle();
        }
        if (resetOrientationButton) {
            ResetOrientation();
        }
    }

    void GetInputs() {
        horizontalInput = Input.GetAxis(gameObject.name + "Horizontal");
        forwardInput = Input.GetAxis(gameObject.name + "Vertical");
        resetOrientationButton = Input.GetButtonDown(gameObject.name + "Select");
        resetPositionButton = Input.GetButtonDown(gameObject.name + "Start");
    }

    void HandleSpeedInput() {
        if (forwardInput > 0) {
            Debug.Log("forward detected");
            // We'll move the vehicle forward
            // move forward if not at max speed
            if (speed < maxSpeed) {
                speed = speed + acceleration;
            }
            // if over max speed, reset to max speed
            if (speed > maxSpeed) {
                speed = maxSpeed;
            }
        } else if (forwardInput < 0) {
            Debug.Log("reverse detected");
            // but if reverse is pressed and forward isn't
            // move it backward
            speed = speed - acceleration;
            // if over max negative speed, reset to max negative speed
            if (speed < -maxSpeed) {
                speed = -maxSpeed;
            }
        } else {
            // Debug.Log("No press, reducing");
            ReduceSpeed();
        }
    }

    void ReduceSpeed() {
        // reset 
        if (speed > 0) {
            speed -= noInputSpeedChange;
        } else if (speed < 0) {
            speed += noInputSpeedChange;
        }
        // avoid oscillations
        if (Mathf.Abs(speed) < noInputSpeedChange) {
            speed = 0;
        }
    }

    void ResetVehicle() {
        transform.position = defaultVehiclePosition;
        speed = 0f;
        ResetOrientation();
    }

    void ResetOrientation() {
        transform.rotation = defaultVehicleRotation;
    }
}
