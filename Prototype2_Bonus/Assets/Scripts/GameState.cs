using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameState : MonoBehaviour
{
    private int lives = 3;
    private int score = 0;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void UpdateLives() {
        lives -= 1;
        Debug.Log("Lost a life");
        if (lives <= 0) {
            Debug.Log("Out of lives. Game Over.");
        }
    }

    public void UpdateScore() {
        score += 1;
        Debug.Log("Score is " + score);
    }
}
