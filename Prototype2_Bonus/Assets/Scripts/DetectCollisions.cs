using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectCollisions : MonoBehaviour
{
    private GameState gameState;
    private ProgressTracker progressTracker;
    private float foodValue = 0.25f;

    void Awake()
    {
        gameState = GameObject.Find("GameState").GetComponent<GameState>();
        progressTracker = transform.GetComponent<ProgressTracker>();
    }

    // Update is called once per frame
    void Update()
    {
    }

    /// <summary>
    /// OnTriggerEnter is called when the Collider other enters the trigger.
    /// </summary>
    /// <param name="other">The other Collider involved in this collision.</param>
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "Player") {
            gameState.UpdateLives();
        } else {
            bool fullyFed = progressTracker.UpdateProgress(foodValue);
            if (fullyFed) {
                gameState.UpdateScore();
                Destroy(gameObject);
            }
            Destroy(other.gameObject);
        }
    }
}
