using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProgressTracker : MonoBehaviour
{
    private Transform progressBox;
    private Transform currentProgress;
    private float maxProgress;
    private Vector3 currentScale;

    void Start()
    {
        progressBox = transform.Find("ProgressBox");
        currentProgress = transform.Find("CurrentProgress");
        currentScale = currentProgress.localScale;
        maxProgress = progressBox.localScale.x;
    }

    // Update is called once per frame
    void Update()
    {
        currentScale = currentProgress.localScale;
    }

    public bool UpdateProgress(float amount) {
        float newAmount = currentScale.x + amount;
        if (newAmount > maxProgress) {
            newAmount = maxProgress;
        }
        Debug.Log("new amount is " + newAmount);
        Debug.Log("max progress is " + maxProgress);
        Vector3 newScale = new Vector3(newAmount, currentScale.y, currentScale.z);
        currentProgress.localScale = newScale;

        if (newScale.x >= maxProgress) {
            Debug.Log("progress is full");
            return true;
        } else {
            return false;
        }
    }
}
